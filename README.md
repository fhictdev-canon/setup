# Setup Guide for the project

## Requirement
* Git is installed
* Docker and docker-compose are installed
* Internet connection required
* Make sure localhost port 3000 & 8080 are available

---

### Before setup 

If the database has no activity in the 7 days, the database will be in sleep mode 

THEN YOU HAVE TO WKAE UP THE DATABASE FIRST 

With the following link you can access to the database

[https://app.planetscale.com/canon/canon-db](https://app.planetscale.com/canon/canon-db)

Email
```
spices-advices.0h@icloud.com
```
Password
```
munfex-jopsaF-gytme8
```


## 1. Clone the frontend and backend repository to the same empty directory

Backend Repo

```bash
git clone https://gitlab.com/fhictdev-canon/s3-grp2-backend.git
```

Frontend Repo

```bash
git clone https://gitlab.com/fhictdev-canon/s3-grp2-frontend.git
```

---
## 2. Create a file named `docker-compose.yml` with the following content and place it in the top level of the directory
 

```bash
version: "3"
services:

  grp-frontend:
    container_name: react-frontend
    build: ./s3-grp2-frontend/
    ports:
      - "3000:3000"
    depends_on:
      - grp-backend

  grp-backend:
    container_name: api-backend
    build: ./s3-grp2-backend/
    ports:
      - "8080:8080"
  
```
If you have problem start the application, please check if the folder name match the name in the build folder, it is important that match

---
## 3. Run the application

Use the following command to run the application in the top level directory
```bash
docker-compose up -d 
```




